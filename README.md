# CLI Tic Tac Toe Game


## Description

Command line Interface version of popular Tic Tac Toe Game written in python.


## Visuals

**look of the game.**

 ![Glimpse](./assets/ss.png)


## Installation

The repository can be 

-  cloned by `git clone https://gitlab.com/Hafijul_ali/cli-tic-tac-toe-game.git`
-  change directory using `cd cli-tic-tac-toe-game`


## Usage

The game is fairly simple to play, use numbers 1 to 9 to place your symbol in the specified position. A web-based version of the game can be played at [Live](https://replit.com/@HafijulAli/CLI-Tic-Tac-Toe-Game?v=1)


## Making changes 

```
cd existing_repo
```
```
git remote add origin https://gitlab.com/Hafijul_ali/cli-tic-tac-toe-game.git
```
```
git branch -M main
```
```
git push -uf origin main
```


## Authors and acknowledgment

This project is solely work of their author(s), any external library used has been properly mentioned in the `requirements.txt` file.


## License

This project is licensed under [Apache License2.0](https://apache.org/licenses/LICENSE-2.0) .


## Project status

This project is a work in progress and changes to the code base is done depending upon author(s)' availability. Any code changes will be reflected in Git Commit History.