__title__ = "Your App Name"
__version__ = "1.0.0"
__authors__ = "Your Project Author(s)"
__authors_email__ = "Your Project Author Email(s)"
__description__ = "Your App short description"
__copyright__ = "Copyright 2023-2024 Author(s), Org / Individual"
__all__ = [
    "common",
    "game",
    "libs",
    "utils",
    "services",
    "views",
]
