LOGO = '''

 _   _      _             _             
| | (_)    | |           | |            
| |_ _  ___| |_ __ _  ___| |_ ___   ___ 
| __| |/ __| __/ _` |/ __| __/ _ \\ / _ \\
| |_| | (__| || (_| | (__| || (_) |  __/
 \\__|_|\\___|\\__\\__,_|\\___|\\__\\___/ \\___|

'''

INTRO = '''Welcome, to the game
basic instructions:-
-There are two game modes. 
1)Human v/s Computer.
2)Human v/s Human.
N.B:- This program doesn't let you play Human v/s Human mode remotely.Those are future plans.

-By default symbol for Player 1 is X and for  Player 2 is O

-The board is a 3x3 grid with each cell numbered from 1 to 9

-The game moves from Player 1 to Player 2 in sequential turn.

-Enter a value(1-9) to place your symbol there.

Like this:-
 ___ ___ ___
|_1_|_2_|_3_|
|_4_|_5_|_6_|
|_7_|_8_|_9_|

Press q or 0 to exit this menu

_______________ENJOY______________

'''
