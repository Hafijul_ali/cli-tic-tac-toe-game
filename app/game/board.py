from random import randint, choice
from time import sleep
from typing import List, Union


class Board:
    def __init__(self, size: int) -> None:
        self.size: int = size
        self.empty_cell_count: int = size * size
        self.symbol: str = "X"
        self.board: List[List[int]] = [
            [" " for x in range(self.size)] for y in range(self.size)
        ]

    def __vertical(self) -> Union[str, None]:
        i = 0
        while i < self.size:
            j = 0
            symbol_count = 0
            while j < self.size:
                if self.board[j][i] == self.symbol:
                    symbol_count += 1
                if symbol_count == self.size:
                    return self.symbol
                j += 1
            i += 1
        return None

    def __horizontal(self) -> Union[str, None]:
        for row in self.board:
            if row.count(self.symbol) == self.size:
                return self.symbol
        return None

    def __leading_diagonal(self) -> int:
        symbol_count = 0
        for i in range(self.size):
            if self.board[i][i] == self.symbol:
                symbol_count += 1
        if symbol_count == self.size:
            return self.symbol
        return None

    def __trailing_diagonal(self) -> int:
        symbol_count = 0
        i = 0
        j = self.size - 1
        while i < self.size and j > -1:
            if self.board[i][j] == self.symbol:
                symbol_count += 1
            j -= 1
            i += 1
        if symbol_count == self.size:
            return self.symbol
        return None

    def __diagonal(self) -> Union[str, None]:
        return self.__leading_diagonal() or self.__trailing_diagonal() or None

    def occupy_cell(self, x: int, y: int) -> None:
        """
        Occupy cell function gets the row and column index value and assigns the required symbol to the position.
        """
        if self.board[x][y] == " ":
            if self.symbol == "O":
                self.symbol: str = "X"
            else:
                self.symbol: str = "O"
            self.board[x][y] = self.symbol
            self.empty_cell_count -= 1
        else:
            print("Cell already occupied")
            sleep(1)
            return

    def match(self):
        return self.__vertical() or self.__horizontal() or self.__diagonal() or "Draw"
