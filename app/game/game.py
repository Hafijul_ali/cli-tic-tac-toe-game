from game.board import Board
from common.constant import LOGO, INTRO
from services.controls import Control
from views.ui import UI
from utils.diskio import DiskIO
from utils.logger import Logger


class Game:
    def __init__(self) -> None:
        self.board: Board = Board(size=3)
        self.input = Control(game=self)
        self.ui = UI(data=self.board)
        self.utils = DiskIO("data.txt")
        self.logger: Logger = Logger("logs")
        self.winner:str = "Draw"
        self.ui.show_help()
        self.ui.display()

    def run(self) -> None:
        """
        Run function to run the main loop of the game. It is resposible for calling :

        read keypress -> understand keypress -> clear the ui -> redraw ui with new values -> repeat
        """
        while not self.over():
            key = self.input.read()
            self.input.handle(key)
            self.ui.wipe()
            self.ui.display()
        print("Game over, result was :", self.result())
        self.logger.info(f"Game over, result was : {self.result()}")

    def over(self) -> bool:
        """
        Over function to check if current game is over or not.

        Game loop breaks on the boolean value returned by this function.
        """
        self.winner = self.board.match()
        if self.board.empty_cell_count == 0 or self.winner in ("X", "O"):
            return True
        return False

    def result(self):
        """
        Result function returns the prettified message for result if any of the player wins, else shows Draw.
        """
        return self.winner + " wins" if self.winner != "Draw" else self.winner
