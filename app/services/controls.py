from libs.keyinput import KBHit
from utils.logger import Logger
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from game.game import Game


class Control:
    def __init__(self, game) -> None:
        self.key: KBHit = KBHit()
        self.game: Game = game

    def read(self) -> str:
        return self.key.getch()

    def handle(self, key: str) -> None:
        try:
            key = int(key) - 1
            self.game.logger.info(f"Key Pressed : {key}")
            if key < 0 or key > 10:
                print("Invalid input, please enter a digit between 1 and 9")
                return
            x = key // self.game.board.size
            y = key % self.game.board.size
            self.game.board.occupy_cell(x, y)
        except (ValueError, TypeError) as e:
            print(f"Unexpected key, {key} was pressed")
            self.game.logger.error(f"Unexpected key, {key} was pressed. Details : {e}")
        except Exception as e:
            self.game.logger.error(e)
