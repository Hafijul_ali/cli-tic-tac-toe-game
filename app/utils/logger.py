import logging
import os


class Logger:
    def __new__(
        self,
        name: str,
        level: int = logging.INFO,
    ) -> logging.Logger:
        if not os.path.exists("logs/"):
            os.mkdir("logs")
        logging.basicConfig(
            filename=f"logs/{name}.log",
            filemode="a",
            format="%(asctime)s - %(levelname)-8s - [line %(lineno)d]: %(message)s",
            level=level,
            force=True,
        )

        return logging.getLogger(name=name)
