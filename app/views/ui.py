import os
from common.constant import INTRO, LOGO


class UI:
    def __init__(self, data) -> None:
        self.data = data

    def display(self) -> None:
        pad = 5
        dash = (pad + 2) * len(self.data.board) + (len(self.data.board) - 1)
        for i in range(len(self.data.board)):
            print("-" * dash)
            print(
                *[
                    "|{:^{pad}}|".format(self.data.board[i][j], pad=pad)
                    for j in range(len(self.data.board))
                ]
            )
            print("-" * dash)

    def wipe(self) -> None:
        if os.name == "nt":
            os.system("cls")
        else:
            os.system("clear")

    def show_help(self) -> None:
        print(LOGO)
        print(INTRO)
